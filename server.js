const { Nuxt, Builder } = require("nuxt");
var express = require("express");
var cors = require("cors");
const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
// We instantiate Nuxt.js with the options
let config = require("./nuxt.config.js");
const nuxt = new Nuxt(config);
app.use(nuxt.render);

// Build only in dev mode
new Builder(nuxt).build();

// Listen the server
app.listen(port, function() {
  console.log(`Server is listening on port: ${port}`);
});
