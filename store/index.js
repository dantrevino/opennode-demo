export const state = () => ({
  signedIn: false,
  userData: {},
  user: {},
  username: '',
  avatarUrl: '',
  loading: false
})

export const mutations = {
  SET_AUTH(state, loggedIn) {
    state.signedIn = loggedIn
  },
  SET_LOADING_STATUS(state, status) {
    state.loadingStatus = status
  },
  SET_USER_DATA(state, ud) {
    state.userData = ud
  },
  SET_USER(state, user) {
    state.user = user
  }
}

export const actions = {
  setAuth(context, status) {
    context.commit('SET_AUTH', status)
  },
  setLoading(context, state) {
    context.commit('SET_LOADING_STATUS', state)
  }
}

export const getters = {
  signedIn: state => state.signedIn,
  userAvatar: state => state.avatarURL
}
