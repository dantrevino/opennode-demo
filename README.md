# opennode-demo

> My posh Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).


## Setting Up Opennode

* log into https://dev.opennode.co/
* Go to 'Settings', 'Integrations' and Add an API Key
* add your api key to the `payment.vue` and `dashboard.vue` pages